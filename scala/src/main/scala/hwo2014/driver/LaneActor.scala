package hwo2014
package driver

import akka.actor._
import data._
import util._

class LaneActor extends Actor with ActorLogging with EventStreamHandler {
  import context._
  import State._
  import Piece._

  object State {
    var car: Option[CarId] = None
    var gameInit: GameInit = _
    var lastCommand: String = ""
  }

  def publishStatus(cmd: String) = {
    if (cmd != lastCommand) {
      lastCommand = cmd
      sender ! Response(MsgWrapper("switchLane", cmd))
    } else { sender ! Response(Ping) }
  }

  implicit lazy val lanes = gameInit.race.track.lanes

  subscribe(classOf[CarId])
  subscribe(classOf[GameInit])

  def receive = {
    case carId: CarId =>
      log.info("received $carId")
      car = Some(carId)

    case gI: GameInit =>
      println("TrackActor : received gameInit")
      gameInit = gI
      log.info(lanes.toString)

    case LifeTrackData(trackData, lfqPos) =>
      val laneType = lfqPos.lane.laneType
      val nextCurve = trackData.findNext(Piece.isAnyCurve _, 1)
      trackData match {
        case Switch(_) :: _ => 
          lastCommand = "" // if we are on a swith the next switch is a new command
          sender ! Response(Ping) // do not attempt to switch on switches
        case AnyCurve(c) :: _ if (c.switch.getOrElse(false)) => 
          sender ! Response(Ping) // do not switch in curves? TODO required?
        case _ =>
          (laneType, nextCurve) match {
            case (RightLane, Some(Piece.Left)) => publishStatus("Left")
            case (LeftLane, Some(Piece.Right)) => publishStatus("Right")
            case (MiddleLane, Some(Piece.Left)) => publishStatus("Left")
            case (MiddleLane, Some(Piece.Right)) => publishStatus("Right")
            case _ => 
              lastCommand = "" 
                sender ! Response(Ping)
          }
      }
  }
}