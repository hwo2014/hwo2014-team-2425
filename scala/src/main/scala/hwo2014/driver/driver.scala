package hwo2014
package driver

import data._
import util._
import java.io._
import akka.actor._
import akka.actor.SupervisorStrategy._

case class RaceManager() extends Actor with ActorLogging with EventStreamHandler {
  import context._

  var input: BufferedReader = _
  
  val car = actorOf(Props[CarActor], "carActor")
  val lane = actorOf(Props[LaneActor], "laneActor")
  val trackActor = actorOf(TrackActor.props(car, lane), "trackActor")

  subscribe(classOf[GameInit])
  
  override val supervisorStrategy =
    AllForOneStrategy(maxNrOfRetries = 0) {
      case e: Exception    => 
        log.error(s"Grave error : ${e.getMessage}")
        log.error("Stacktrace")
        log.error(e.getStackTraceString)
        Escalate
    }

  def receive = {

    // join a track
    case join: Join =>
      println("RaceManager : received join request")
      publish(join)

    case joinRace: JoinRace =>
      println("RaceManager : received joinRace request")
      publish(joinRace)
      
    case joinRace: CreateRace =>
      println("RaceManager : received createRace request")
      publish(joinRace)

    case gameInit: GameInit =>
      scala.concurrent.Future {
        val logName = gameInit.race.track.name + ".csv"
        val log: PrintWriter = new PrintWriter(logName, "UTF8")
        log.println(s"Index  \t Type \t Length \t Switch \t Radius \t Angle")
        gameInit.race.track.pieces.zipWithIndex.foreach { case (p, index) => log.println(index + "\t" + p.toString) }
        log.flush()
        log.close()
      }
  }
}
