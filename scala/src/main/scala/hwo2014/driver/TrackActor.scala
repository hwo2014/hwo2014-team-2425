package hwo2014
package driver

import data._
import util._
import scala.concurrent._
import scala.concurrent.duration._
import java.io.PrintWriter
import akka.actor._
import akka.util._
import akka.pattern._
import java.io.BufferedReader

case class TrackActor(carActor: ActorRef, laneActor: ActorRef) extends Actor with ActorLogging with EventStreamHandler {
  import context._
  import State._

  object State {
    var gameInit: GameInit = _
    var currentPositions: CarPositions = CarPositions(List.empty, 0)
    var lofiQueen: CarId = _
    var lastCarCommand: Option[Response] = None
    var lastLaneCommand: Option[Response] = None
    var commandPipeLine: List[Response] = List.empty
  }

  implicit val timeOut: Timeout = 100.milliseconds

  subscribe(classOf[GameInit])
  subscribe(classOf[CarId])
  subscribe(classOf[CarPositions])
  subscribe(classOf[Crash])
  subscribe(classOf[Spawn])

  def lofiQueenPos() = currentPositions.findCar(lofiQueen)

  def receive = {
    // get the track information
    case gI: GameInit =>
      println("TrackActor : received gameInit")
      gameInit = gI

    // get current car positions
    case curPos: CarPositions =>
      // println("TrackActor : received carPositions")
      currentPositions = curPos
      val gameTick = curPos.gameTick
      lofiQueenPos().map { lfqPos =>
        val trackData = gameInit.trackAheadForCar(lfqPos)
        val (trackInfo, _) = trackData.unzip

        val carResult = (carActor ? LifeTrackData(trackInfo, lfqPos)).mapTo[Response]
        val laneResult = (laneActor ? LifeTrackData(trackInfo, lfqPos)).mapTo[Response]
        val totalResult = carResult zip laneResult
        val (cRes, lRes) = Await.result(totalResult, Duration.Inf) // must AWAIT here, to keep in sync with the server

        commandPipeLine = (commandPipeLine ++ List(cRes, lRes)).filter(result => result != Response(Ping))
        commandPipeLine match {
          case Nil => publish(Ping)
          case head :: tail =>
            commandPipeLine = tail
            publish(head.data)
        }

      }

    // get our car id
    case carId: CarId =>
      println("TrackActor : received carId")
      lofiQueen = carId

    // handle crash

    case crash: Crash =>
      val crashedCar = CarId(crash.name, crash.color)
      println(s"TrackActor : received $crashedCar")
      if (lofiQueen != null && lofiQueen == crashedCar) carActor ! crash

    //handle spawn
    case spawn: Spawn =>
      println(s"TrackActor : received $spawn")
      val spawnedCar = CarId(spawn.name, spawn.color)
      if (lofiQueen != null && lofiQueen == spawnedCar) carActor ! spawn

  }
}

object TrackActor {
  def props(car: ActorRef, lane: ActorRef) = Props(classOf[TrackActor], car, lane)
}

case class Response(data: AnyRef)