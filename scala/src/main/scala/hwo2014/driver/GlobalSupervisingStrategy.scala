package hwo2014.driver

import akka.actor._
import akka.actor.SupervisorStrategy._
import akka.actor.AllForOneStrategy

class GlobalSupervisingStrategy extends SupervisorStrategyConfigurator {
  def create(): SupervisorStrategy = {
    AllForOneStrategy(maxNrOfRetries = 0, loggingEnabled = true) {
      case e: Throwable => Escalate
    }
  }
}