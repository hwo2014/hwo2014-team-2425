package hwo2014
package driver

import java.io.BufferedReader
import java.io.PrintWriter
import akka.actor._
import data._
import util._
import akka.event._
import scala.concurrent.duration._

import org.json4s._
import org.json4s.DefaultFormats
import org.json4s.native.Serialization
import org.json4s.native.JsonMethods._

trait EventStreamHandler {
  this: Actor =>
  lazy val eventStream: EventStream = context.system.eventStream
  def publish(msg: AnyRef): Unit = {
    eventStream.publish(msg)
  }
  def subscribe(clazz: Class[_]) = {
    println(s"${self} : subscribed to ${clazz.getSimpleName()}")
    eventStream.subscribe(self, clazz)
  }
}

class IO(reader: BufferedReader, writer: PrintWriter) extends Actor with EventStreamHandler {

  import context._
  import State._

  object State {
    var gameInit: GameInit = _
  }

  implicit val implicitWriter = writer
  implicit val formats = new DefaultFormats {}

  val watchDog = actorOf(Props[WatchDog], "watchDog")

  subscribe(classOf[MsgWrapper])
  subscribe(classOf[Join])
  subscribe(classOf[JoinRace])
  subscribe(classOf[CreateRace])

  def query() = {
    system.scheduler.scheduleOnce(10 milliseconds) {
      self ! Tick
      watchDog ! Tick
    }
  }


  def receive = {
    case jm: Join =>
      println(s"IO : Sending join $jm to server")
      MsgWrapper("join", jm).send()
      query()

    case jrace: CreateRace =>
      println(s"IO : Sending create race request $jrace to server")
      MsgWrapper("createRace", jrace).send()
      query()

    case jrace: JoinRace =>
      println(s"IO : Sending join race request $jrace to server")
      MsgWrapper("joinRace", jrace).send()
      query()

    case m @ MsgWrapper("join", _) =>
      println("IO : Sending join to server")
      m.send()
      query()

    case m @ MsgWrapper(msg, data) =>
      //println(s"IO : Sending $msg -> $m")
      m.send()

    case "Die" =>
      system.shutdown

    case Tick =>
      val line = reader.readLine()
      if (line != null) {
        val message = Serialization.read[MsgWrapper](line)
        val json = parse(line)
        message match {
          case MsgWrapper("error", error) =>
            val e = error.extract[String]
            println(s"IO : received -> $error")
            publish(e)
            if (e.startsWith("Invalid track")) system.shutdown()
            Ping.send()
            
          case MsgWrapper("carPositions", carPositionsData) =>
            val json = parse(line)
            
            val carPositions: List[CarPosition] = carPositionsData.extract[List[CarPosition]]
            publish(CarPositions(carPositions, 0))
            
          case MsgWrapper("yourCar", yourCarData) =>
            val yourCar: CarId = yourCarData.extract[CarId]
            println(s"IO : received -> yourCar")
            publish(yourCar)
            Ping.send()
          
          case MsgWrapper("gameInit", gameInitData) =>
            println("IO : received -> gameInit")
            gameInit = gameInitData.extract[GameInit]
            publish(gameInit)
            Ping.send()
          
          case MsgWrapper("gameStart", _) =>
            publish(GameStart)
            watchDog ! "on"
            Ping.send()
          
          case MsgWrapper("gameEnd", _) =>
            println(s"IO : received -> gameEnd")
            watchDog ! "off"
            Ping.send()
          
          case MsgWrapper("tournamentEnd", _) =>
            println(s"IO : received -> tournamentEnd")
            Ping.send()
            system.shutdown()
          
          case MsgWrapper("crash", crashData) =>
            val crash: Crash = crashData.extract[Crash]
            publish(crash)
            Ping.send()
          
          case MsgWrapper("spawn", spawnData) =>
            val spawn: Spawn = spawnData.extract[Spawn]
            publish(spawn)
            Ping.send()
          
          case MsgWrapper(msgType, data) =>
            println(s"""
                
                
                IO : Received:  $msgType -> $data
                
                
                """)
            Ping.send()
        }
        // we responded, hence query again and notify the watchdog
        query()
      } else {
        // socket was null - no server input, repeat polling
        query()
      }
  }
}

case object Tick