package hwo2014
package driver

import scala.concurrent.duration._
import akka.actor._
import data._
import akka.event._
class WatchDog extends Actor with ActorLogging {

  import context._
  
  var dog : Cancellable = _

  def off: Actor.Receive = {
    case "on" =>
      dog = system.scheduler.scheduleOnce(30 seconds, self, "Die")
      log.info("Turning watchdog on!")
      become(on)
    case _ =>
  }

  def on: Actor.Receive = {
    case "off" => 
      log.info("Turning watchdog off!")
      dog.cancel()
      become(off)
    case "Die" => throw new Exception("Die, die, die")
    case Tick => 
      dog.cancel()
      dog = system.scheduler.scheduleOnce(30 seconds, self, "Die")
  }

  def receive = off
}
