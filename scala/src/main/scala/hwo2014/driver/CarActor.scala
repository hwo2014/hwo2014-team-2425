package hwo2014
package driver

import data._
import Piece._
import akka.actor._
import scala.math.abs

case class CarActor() extends Actor with ActorLogging with EventStreamHandler {

  implicit val epsilon: Double = 0.00001
  import context._
  import util._
  import State._

  object State {
    var car: Option[CarId] = None
    var savedPositions: Vector[CarPosition] = Vector.empty
    var currentSpeed = 0.0
    var maxSpeed = 1.0
    var crashed = false
    var lastSpeed = -1.0
  }

  subscribe(classOf[CarId])
  subscribe(GameStart.getClass())

  def publishSpeed() = {
    if (lastSpeed =~= currentSpeed)
      sender ! Response(Ping)
    else {
      lastSpeed = currentSpeed
      sender ! Response(MsgWrapper("throttle", currentSpeed))
    }
  }

  def xyz() = {
    val angles = savedPositions.dropWhile(_.angle =~= 0).take(3).map(_.angle)
  }

  def publishStatus() = if (crashed) sender ! Response(Ping) else publishSpeed()

  def receive = {
    case LifeTrackData(trackData, lfqPos) =>
      savedPositions = savedPositions :+ lfqPos
      val angle = lfqPos.angle
      def angleFac(slowNess: Double = 10) = (1 / (1 + (abs(angle) / slowNess)))
      def ratio(p: Piece) = {
        for {
          a <- p.angle
        } yield { abs(p.pieceLength / a) }
      }

      trackData match {
        // straight line
        case Straight(_) :: Straight(_) :: Straight(_) :: _ => currentSpeed = 1.0
        case Straight(_) :: Switch(s) :: _ if (s.length.isDefined) => currentSpeed = 0.9 * maxSpeed
        case Straight(_) :: Straight(_) :: _ => currentSpeed = 0.8 * maxSpeed
        case Straight(_) :: AnyCurve(c) :: _ if (ratio(c).get <= 50) => currentSpeed = 0.1 * angleFac(2) * maxSpeed
        case Straight(_) :: AnyCurve(c) :: _ if (ratio(c).get <= 100) => currentSpeed = 0.2 * angleFac(4) * maxSpeed
        case Straight(_) :: AnyCurve(c) :: _ => currentSpeed = 0.5 * angleFac() * maxSpeed
        case AnyCurve(c) :: Straight(_) :: _ if (ratio(c).get <= 50)=> currentSpeed = 0.3 * angleFac() * maxSpeed
        case AnyCurve(c) :: Straight(_) :: _ if (ratio(c).get <= 100) => currentSpeed = 0.4 * angleFac() * maxSpeed
        case AnyCurve(c) :: Straight(_) :: _ => currentSpeed = 0.9 * angleFac() * maxSpeed
        case AnyCurve(c) :: AnyCurve(_) :: _ if (ratio(c).get <= 50) => currentSpeed = 0.2 * angleFac(1) * maxSpeed
        case AnyCurve(c) :: AnyCurve(_) :: _ if (ratio(c).get <= 100) => currentSpeed = 0.3 * angleFac(1) * maxSpeed
        case AnyCurve(c) :: AnyCurve(_) :: _ => currentSpeed = 0.4 * angleFac(1) * maxSpeed
      }

      val (cur :: next :: _) = trackData
      log.info(s"${"%1.4f" formatLocal (en, currentSpeed)} \t ${cur.typeOf} \t ${next.typeOf} \t ${"%2.4f" formatLocal (en, angle)} \t $crashed ")
      publishStatus()

    // get my own car id
    case carId: CarId =>
      log.info("received $carId")
      car = Some(carId)
      publish(Ping)
    case crash: Crash =>
      log.info(s"""
          
          received Crash notice !!!
          $crash
          
          """)
      maxSpeed = maxSpeed * 0.9
      crashed = true
      publish(Ping)
    case spawn: Spawn =>
      crashed = false
      publish(Ping)
  }

  object LaneStatus {
    var currentLane: Lane = _
    var issuedSwitchCommand: Boolean = false
  }
}

object CarActor {
  def props() = Props[CarActor]
}