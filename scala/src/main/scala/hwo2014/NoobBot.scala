package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{ BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter }
import org.json4s.native.Serialization
import org.json4s.native.JsonMethods._
import data._
import driver._
import scala.annotation.tailrec
import akka.actor._
import scala.util.Try

object NoobBot extends App {
  
  println(args.toList)
  args.toList match {
    case hostName :: port :: botName :: botKey :: Nil =>
      status("Send single join request")
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey, Join(botName, botKey))

    case hostName :: port :: botName :: botKey :: track :: numbers :: "joinRace" :: Nil =>
      status("Send race join request")
      val uniqueBot = botName + java.util.UUID.randomUUID().toString().substring(0,5)
      new NoobBot(hostName, Integer.parseInt(port), uniqueBot, botKey, JoinRace(BotId(uniqueBot, botKey), Some(track), None, numbers.toInt))
    case _ => status(s"args missing -> ${args.toList}", Console.RED)

  }

  lazy val actorSystem = ActorSystem("hwo")
  lazy val raceManager = actorSystem.actorOf(Props[RaceManager], "raceManager")
}

class NoobBot(host: String, port: Int, botName: String, botKey: String, msg: AnyRef) {
  import NoobBot._
  implicit val formats = new DefaultFormats {}
  println("""
      
 _          _____ _    ___                            
| |    ___ |  ___(_)  / _ \ _   _  ___  ___ _ __  ___ 
| |   / _ \| |_  | | | | | | | | |/ _ \/ _ \ '_ \/ __|
| |__| (_) |  _| | | | |_| | |_| |  __/  __/ | | \__ \
|_____\___/|_|   |_|  \__\_\\__,_|\___|\___|_| |_|___/
""")
                                                      
  try {
    val socket = new Socket(host, port)
    val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
    val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
    val io = actorSystem.actorOf(Props(classOf[IO], reader, writer), "io")
    
    raceManager ! msg
  } catch {
    case t: Throwable => println(s"""|${Console.BOLD}Exception in LoFi Queen Bot${Console.RESET}
        |
        |${Console.RED}Stacktrace:
        |-----------${Console.RESET}
        |
        |${t.getStackTraceString}
    """.stripMargin)
    actorSystem.shutdown()
  }
}



