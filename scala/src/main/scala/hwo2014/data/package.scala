package hwo2014

import org.json4s._

package object data {
  /**
   * Compare a double with implicit precision.
   */
  implicit class DoubleOps(self: Double) {
    def =~=(that: Double)(implicit epsilon: Double) = math.abs(self - that) < epsilon
  }

  /**
   * Shortcut for PING messages.
   */
  val Ping = MsgWrapper("ping", JNull)

  implicit class PieceOps(self: Piece) {
    def pieceLength() = (for {
      a <- self.angle
      r <- self.radius
    } yield (math.abs(a * r))).getOrElse(self.length.get)
  }

  /**
   * Operations on List of pieces aka the track or lookahead.
   *
   */
  implicit class TrackOps(self: List[Piece]) {
    def findNext(pred: Piece => Boolean, start: Int): Option[Piece.TrackInfo] = {
      val pieceIndex = self.indexWhere(
        pred, start)
      if (pieceIndex == -1) None else Some(self(pieceIndex).typeOf())
    }
  }

  /**
   * Operations on the lane of the car.
   */
  implicit class Lane2Ops(self: Lane2) {
    def laneType(implicit lanes: List[Lane]): LaneType = {
      lanes(self.startLaneIndex) match {
        case LeftLane(_) => LeftLane
        case RightLane(_) => RightLane
        case MiddleLane(_) => MiddleLane
      }
    }
  }
}