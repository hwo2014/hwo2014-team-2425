package hwo2014
package data

import org.json4s._
import org.json4s.DefaultFormats
import org.json4s.native.Serialization
import org.json4s.native.JsonMethods._
import java.io._

case class MsgWrapper(msgType: String, data: JValue) {
  def send()(implicit writer: PrintWriter) {
    implicit val formats = new DefaultFormats {}
    //println(s"sending -> $msg")
    writer.println(Serialization.write(this))
    writer.flush
  }
}

object MsgWrapper {
  implicit val formats = new DefaultFormats {}
  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
  
}