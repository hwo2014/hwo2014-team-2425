package hwo2014
package data

import org.json4s._

/**
 * Join Message
 */
case class Join(name: String, key: String)

/** CarId message component. */
case class CarId(name: String, color: String)

/** Speed message */
case class Throttle(throttle: Double)

/** List of Carpositions. */

case class CarPositions(curPos: List[CarPosition], gameTick: Int) {
  def findCar(car: CarId) = curPos.find(_.id == car)
}

/**
 * Single carposition.
 */
case class CarPosition(id: CarId, angle: Double, piecePosition: PiecePosition) {
  val lane = piecePosition.lane
}

/**
 * Track description.
 */
case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: Lane2, lap: Int)

/**
 * Static description of a track lane.
 */
case class Lane(distanceFromCenter: Double, index: Int)

sealed trait LaneType
case object LeftLane extends LaneType {
  def unapply(lane: Lane) = if (lane.distanceFromCenter < 0) Some(lane) else None
}

case object RightLane extends LaneType {
  def unapply(lane: Lane) = if (lane.distanceFromCenter > 0) Some(lane) else None
}

case object MiddleLane extends LaneType {
  implicit val epsilon: Double = 0.001
  def unapply(lane: Lane) = if (lane.distanceFromCenter =~= 0) Some(lane) else None
}

case class Lane2(startLaneIndex: Int, endLaneIndex: Int) {
  val isSwitching = startLaneIndex != endLaneIndex
}

/**
 * Game init message.
 */
case class GameInit(race: Race) {
  val trackData: List[Piece] = race.track.pieces

  def trackInfo =
    trackData map { p =>
      p match {
        case Piece.Straight(p) => (Piece.Straight)
        case Piece.Left(p) => (Piece.Left)
        case Piece.Right(p) => (Piece.Right)
      }
    }

  val trackAheadForCar: CarPosition => List[(Piece, Piece.TrackInfo)] = {
    carPosition: CarPosition =>
      val (head, tail) = (trackData zip trackInfo).splitAt(carPosition.piecePosition.pieceIndex)
      tail ++ head
  }

  val lanes = race.track.lanes

  def isQuickRace() = race.raceSession.isQuickRace
}

case class Race(track: Track, cars: List[Car], raceSession: RaceSession)

case class Track(
  id: String,
  name: String,
  pieces: List[Piece],
  lanes: List[Lane],
  startingPoint: StartingPoint)

case class Piece(
  length: Option[Double],
  switch: Option[Boolean],
  radius: Option[Double],
  angle: Option[Double],
  bridge: Option[Boolean]) {

  def typeOf() = {
    import Piece._
    this match {
      case Switch(_) => Switch
      case Straight(_) => Straight
      case Left(_) => Left
      case Right(_) => Right
    }
  }
  override def toString() = {
       typeOf() + 
       "\t(" + ("%2.1f" formatLocal(en, this.pieceLength())) + ")" + 
       "\t" + switch.getOrElse("n/a") + 
       "\t" + radius.getOrElse("n/a") + 
       "\t" + angle.getOrElse("n/a")
  }  
}

object Piece {
  sealed trait TrackInfo
  case object Straight extends TrackInfo {
    def unapply(p: Piece): Option[Piece] = p match {
      case Piece(Some(length), _, _, _, _) => Some(p)
      case _ => None
    }
  }
  case object Switch extends TrackInfo {
    def unapply(p: Piece): Option[Piece] = p match {
      case Piece(Some(length), Some(true), _, _, _) => Some(p)
      case _ => None
    }
  }
  case object Left extends TrackInfo {
    def unapply(p: Piece): Option[Piece] = p match {
      case Piece(_, _, Some(radius), Some(angle), _) if (angle < 0) => Some(p)
      case _ => None
    }
  }

  case object Right extends TrackInfo {
    def unapply(p: Piece): Option[Piece] = p match {
      case Piece(_, _, Some(radius), Some(angle), _) if (angle > 0) => Some(p)
      case _ => None
    }
  }

  case object AnyCurve extends TrackInfo {
    def unapply(p: Piece): Option[Piece] = p match {
      case Left(_) | Right(_) => Some(p)
      case _ => None
    }
  }

  def isStraight(p: Piece) = p match {
    case Straight(_) => true
    case _ => false
  }

  def isAnyCurve(p: Piece) = p match {
    case AnyCurve(_) => true
    case _ => false
  }
}

case class Lookahead(lahp: Piece.TrackInfo, piece: Piece)
case class Lookaheads(list: Lookahead)

case class StartingPoint(position: Position, angle: Double)
case class Position(x: Double, y: Double)
case class Car(id: CarId, dimensions: CarDimension)
case class CarDimension(length: Double, width: Double, guideFlagPosition: Double)
case class RaceSession(durationMs: Option[String], laps: Option[Double], maxLapTimeMs: Option[Double], quickRace: Option[Boolean]) {
  def isQuickRace() = quickRace.getOrElse(false)
}

case class Crash(name: String, color: String)
case class Spawn(name: String, color: String)
case class Error(msg: JValue)
case object GameStart

case class CreateRace(botId: BotId, trackName: String, password: Option[String], carCount: Int)
case class JoinRace(botId: BotId, trackName: Option[String], password: Option[String], carCount: Int)
case class BotId(name: String, key: String)

case class LifeTrackData(trackData: List[Piece], carPos: CarPosition)


